﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THSchool.API.Models
{
    public class ClassRoom
    {
        public int ClassRoomID { get; set; }
        public string ClassName { get; set; }
    }
}
