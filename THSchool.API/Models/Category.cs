﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace THSchool.API.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }
        [Required]
        public string CategoryName { get; set; }
        public bool IsActive { get; set; }
        public DateTime TimeCreated { get; set; }
        public int PersonCreatedUserId { get; set; }
        public DateTime TimeUpdate { get; set; }
    }
}
