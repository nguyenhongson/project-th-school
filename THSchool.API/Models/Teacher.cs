﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace THSchool.API.Models
{
    public class Teacher
    {

        public int UserId { get; set; }
        [Key]
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
       
    }
}
