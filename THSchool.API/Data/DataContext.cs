﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using THSchool.API.Models;

namespace THSchool.API.Data
{
    public class DataContext:DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
        public DbSet<ClassRoom> ClassRoom { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Teacher> Teacher { get; set; }
        public DbSet<Photo> Photo { get; set; }
        public DbSet<Category> Category { get; set; }
    }
}
