﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using THSchool.API.Helpers;
using THSchool.API.Models;

namespace THSchool.API.Data
{
   public interface ITHSchoolRepository
    {
        //add,delete,savechange,update
        void Add<T>(T entity) where T : class;

        void Delete<T>(T entity) where T: class;

        Task<bool> SaveAll();

        Task<User> GetUser(int id);

        //Task<IEnumerable<User>> GetUsers();
        Task<PagedList<User>> GetUsers(UserParams userParams);

        void UpdateUser(User user);

    }
}
