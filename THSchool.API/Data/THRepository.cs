﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using THSchool.API.Helpers;
using THSchool.API.Models;

namespace THSchool.API.Data
{
    public class THRepository : ITHSchoolRepository
    {
        private readonly DataContext _context;
        public THRepository(DataContext context)
        {
            _context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
            // _context.SaveChanges();
        }

        public async Task<User> GetUser(int id)
        {
            var user = await _context.User.Include(n => n.Photos).FirstOrDefaultAsync(k => k.UserId == id);
            return user;
        }

        //public async Task<IEnumerable<User>> GetUsers()
        //{
        //    var users = await _context.User.Include(n => n.Photos).ToListAsync();
        //    return users;
        //}
        public async Task<PagedList<User>> GetUsers(UserParams userParams)
        {
            var listUsers = _context.User.Include(n => n.Photos);
            return await PagedList<User>.CreateAsync(listUsers, userParams.PageNumber, userParams.PageSize);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void UpdateUser(User user)
        {
            User oneUser = _context.User.Where(n => n.UserId == user.UserId).FirstOrDefault();
            if (oneUser != null)
            {
                oneUser.Username = user.Username;
                oneUser.City = user.City;
                oneUser.DateOfBirth = user.DateOfBirth;
                oneUser.Gender = user.Gender;
                oneUser.Introduction = user.Introduction;
                oneUser.Photos = user.Photos;
                _context.Entry(oneUser).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }
    }
}
