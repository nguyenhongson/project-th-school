﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THSchool.API.Dtos
{
    public class UserForDetailedDto
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public DateTime Created { get; set; }
        public string Introduction { get; set; }
        public string City { get; set; }

        public string PhotoUrl { get; set; }
    }
}
