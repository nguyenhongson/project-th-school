﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THSchool.API.Dtos
{
    public class CategoryForCreateDto
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool IsActive { get; set; }
        public DateTime TimeCreated { get; set; }
        public int PersonCreatedUserId { get; set; }
        public DateTime TimeUpdate { get; set; }
    }
}
