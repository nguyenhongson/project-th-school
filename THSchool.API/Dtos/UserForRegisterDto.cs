﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace THSchool.API.Dtos
{
    public class UserForRegisterDto
    {
        [Required]
        public string  Username { get; set; }
        [Required]
        [StringLength(20, MinimumLength =4, ErrorMessage ="Password must between 4 and 20 characters")]
        public string Password { get; set; }
    }
}
