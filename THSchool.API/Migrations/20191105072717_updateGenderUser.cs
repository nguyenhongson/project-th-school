﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace THSchool.API.Migrations
{
    public partial class updateGenderUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Genger",
                table: "User",
                newName: "Gender");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Gender",
                table: "User",
                newName: "Genger");
        }
    }
}
