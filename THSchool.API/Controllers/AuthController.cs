﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using THSchool.API.Data;
using THSchool.API.Dtos;
using THSchool.API.Models;

namespace THSchool.API.Controllers
{
    [Route("api/[controller]")]
   [ApiController]
    public class AuthController : Controller
    {
        private readonly IAuthRepository _repo;
        private readonly IConfiguration _config;
        public AuthController(IAuthRepository repo,IConfiguration config)
        {
            _repo = repo;
            _config = config;
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserForRegisterDto userForRegisterDto)
        {
            

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            userForRegisterDto.Username = userForRegisterDto.Username.ToLower();
            if(await _repo.UserExits(userForRegisterDto.Username)) { return BadRequest("Username already exits"); }
            var userToCreate = new User
            {
                Username = userForRegisterDto.Username

            };

            var createdUser =  _repo.Register(userToCreate, userForRegisterDto.Password);

            return StatusCode(201);
          
        }

        [HttpPost("login")]
        
        public async Task<IActionResult> Login(UserForLoginDto userForLoginDto)
        {
            //try
            //{
               // throw new Exception("xin chao ban");
                var userFromRepo = await _repo.Login(userForLoginDto.Username, userForLoginDto.Password);
                if (userFromRepo == null)
                {
                    return Unauthorized();
                }
                var claims = new[]
                {
                new Claim(ClaimTypes.NameIdentifier, userFromRepo.UserId.ToString()),
                new Claim(ClaimTypes.Name, userFromRepo.Username)
            };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddDays(1),
                    SigningCredentials = creds
                };
                var tokenHandler = new JwtSecurityTokenHandler();

                var token = tokenHandler.CreateToken(tokenDescriptor);
                return Ok(new
                {
                    token = tokenHandler.WriteToken(token)
                });
           // }
            //catch
            //{
            //    return StatusCode(500, "Something went wrong, please try againnn");
            //}
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}