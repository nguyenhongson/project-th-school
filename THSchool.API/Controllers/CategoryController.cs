﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using THSchool.API.Data;
using THSchool.API.Models;

namespace THSchool.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ITHSchoolRepository _repo;
        private readonly IMapper _mapper;
        public DataContext _context;
        public CategoryController(ITHSchoolRepository repo, IMapper mapper, DataContext context)
        {
            _repo = repo;
            _mapper = mapper;
            _context = context;
        }
        [HttpPost]
        public async Task<IActionResult> CreateCategory(Category category)
        {
            category.TimeCreated = DateTime.Now;
            category.TimeUpdate = DateTime.Now;
            _repo.Add(category);
              await  _repo.SaveAll();
            return Ok();
        }
    }
}