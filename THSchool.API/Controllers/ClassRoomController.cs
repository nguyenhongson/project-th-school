﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using THSchool.API.Data;

namespace THSchool.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class ClassRoomController : ControllerBase
    {
        private readonly DataContext _context;
        public ClassRoomController(DataContext dataContext)
        {
            _context = dataContext;
        }

        public IActionResult GetListClassRoom()
        {
            var listClass = _context.ClassRoom.ToList();
            return Ok(listClass);
        }

    }
}