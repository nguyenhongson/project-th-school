﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using THSchool.API.Data;
using THSchool.API.Dtos;
using THSchool.API.Helpers;

namespace THSchool.API.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class SeeListOrderController : ControllerBase
    {
       
        private readonly ITHSchoolRepository _repo;
        private readonly IMapper _mapper;
        public DataContext _context1;
        public SeeListOrderController(ITHSchoolRepository repo, IMapper mapper, DataContext context1)
        {
            _mapper = mapper;
            _repo = repo;
            _context1 = context1;
        }
        //[HttpDelete]
        //[Route("API/Quan_Ly_SV/{id}")]
        [HttpGet]
        [Route("api/seelistorder/{id}")]

        public async Task<IActionResult> GetListOrderByUserId(int id )
        {
            //var user = await _repo.GetUser(id);
            //var UserViewModel = _mapper.Map<UserForDetailedDto>(user);
            //return Ok(UserViewModel);

            //dang get toan bo user/ sau thay bang get list order by id user

            UserParams userParams = new UserParams();
            var users = await _repo.GetUsers(userParams);
            var UsersViewModel = _mapper.Map<IEnumerable<UserForListDto>>(users);
           
            Response.AddPagination(users.CurrentPage, users.PageSize, users.TotalCount, users.TotalPages);

            return Ok(UsersViewModel);
        }





        //giả sử trong dropdown, chọn 1 user, hiển thị những mặt hàng mà user đó đã mua
        //ở đây chọn 1 user:  hiển thị list user 

        [HttpGet]
        [Route("api/getoneuserbyuserid/{id}")]
        //[HttpGet("{id}")]
        public async Task<IActionResult> GetOneUserByUserId(int id)
        {
            var user = await _repo.GetUser(id);
            var UserViewModel = _mapper.Map<UserForDetailedDto>(user);
            return Ok(UserViewModel);

            //dang get toan bo user/ sau thay bang get list order by id user
            //var users = await _repo.GetUsers();
            //var UsersViewModel = _mapper.Map<IEnumerable<UserForListDto>>(users);
            //return Ok(UsersViewModel);
        }



    }
}