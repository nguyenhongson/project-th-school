﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using THSchool.API.Data;
using THSchool.API.Dtos;
using THSchool.API.Helpers;
using THSchool.API.Models;

namespace THSchool.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ITHSchoolRepository _repo;
        private readonly IMapper _mapper;
        public DataContext _context1;
        public UsersController(ITHSchoolRepository repo, IMapper mapper, DataContext context1)
        {
            _mapper = mapper;
            _repo = repo;
            _context1 = context1;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers([FromQuery]UserParams userParams)
        {
            var users = await _repo.GetUsers(userParams);
            var UsersViewModel = _mapper.Map<IEnumerable<UserForListDto>>(users);
            Response.AddPagination(users.CurrentPage, users.PageSize,users.TotalCount,users.TotalPages);
            return Ok(UsersViewModel);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            var user = await _repo.GetUser(id);
            var UserViewModel = _mapper.Map<UserForDetailedDto>(user);
            return Ok(UserViewModel);
        }

       
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOneUser(int id)
        {
            User user = await _repo.GetUser(id);
                _repo.Delete(user);
            return Ok();
        }

        //update User
        [HttpPut]
        public IActionResult UpdateUser( User user)
        {
            int m = 1;
           _repo.UpdateUser(user);
            return Ok();
        }
        [HttpDelete]
        public async Task<IActionResult> deleteMany()
        {
            var xyz = HttpContext.Request?.Headers["aaa"];
            dynamic obj = JsonConvert.DeserializeObject(xyz);
            var muki = obj;
            foreach(var item in muki)
            {
                string mula = item;
                int idUser = Convert.ToInt32(item);
                User user = await _repo.GetUser(idUser);
                _repo.Delete(user);
            }
            if(await _repo.SaveAll())
            return Ok();
            return BadRequest("hekllo");
        }
    }
}