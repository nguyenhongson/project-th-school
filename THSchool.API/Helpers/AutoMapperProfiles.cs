﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using THSchool.API.Dtos;
using THSchool.API.Models;

namespace THSchool.API.Helpers
{
    public class AutoMapperProfiles: Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForListDto>()
                .ForMember(n => n.PhotoUrl, k =>
                {
                    k.MapFrom(i => i.Photos.FirstOrDefault(n => n.IsMain).Url);
                })
                .ForMember(n => n.Age, k =>
                {
                    k.ResolveUsing(n => n.DateOfBirth.Caculator());
                });
            CreateMap<User, UserForDetailedDto>().ForMember(n => n.PhotoUrl, k =>
            {
                k.MapFrom(i => i.Photos.FirstOrDefault(n => n.IsMain).Url);
            })
            .ForMember(n => n.Age, k =>
            {
                k.ResolveUsing(n => n.DateOfBirth.Caculator());
            });
            CreateMap<Photo, PhotoForDetailedDto>();

        }
    }
}
