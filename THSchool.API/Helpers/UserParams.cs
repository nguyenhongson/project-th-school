﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THSchool.API.Helpers
{
    public class UserParams
    {
        public int PageNumber { get; set; } = 1;
        public const int MaxPageSize = 50;
        public int pageSize = 7;
        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = (value > MaxPageSize) ? MaxPageSize : value; }
        }
    }
}
